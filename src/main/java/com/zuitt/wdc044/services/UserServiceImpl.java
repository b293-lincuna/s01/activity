package com.zuitt.wdc044.services;

import com.zuitt.wdc044.models.Post;
import com.zuitt.wdc044.models.User;
import com.zuitt.wdc044.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.zuitt.wdc044.config.JwtToken;

import java.util.Optional;

@Service
public class UserServiceImpl implements UserService{

    @Autowired
    private UserRepository userRepository;

    @Autowired
    JwtToken jwtToken;

    public void createUser(User user){
        //Saves the user in our User Table
        userRepository.save(user);
    }

    public Optional<User> findByUsername(String username){
        return Optional.ofNullable(userRepository.findByUsername(username));
    }

    //Mini-Activity
    //Modify the UserController and create a getUser() function that retrieves a specific user using the token, when a request is received at the "/userProfile" endpoint.
    public ResponseEntity getUser(String stringToken) {

        /* MINI ACTIVITY
        User user = userRepository.findByUsername(jwtToken.getUsernameFromToken(stringToken));
        User userDetails = userRepository.findById(user.getId()).get();

        userDetails.setPassword("");
        return new ResponseEntity<>(userDetails, HttpStatus.OK) ;
        */

        /* Solution
        * https://gitlab.com/RupertRamos/b293-batch-resources
        * */
        User user = userRepository.findByUsername(jwtToken.getUsernameFromToken(stringToken));
        user.setPassword("");
        return new ResponseEntity<>(user, HttpStatus.OK) ;

    }

}
