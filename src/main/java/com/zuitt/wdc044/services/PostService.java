package com.zuitt.wdc044.services;

import com.zuitt.wdc044.models.Post;
import org.springframework.http.ResponseEntity;

import java.util.Optional;

public interface PostService {
    void createPost(String stringToken, Post post);

    Iterable<Post> getPosts();

    //edit a user's post
    ResponseEntity updatePost(Long id, String stringToken, Post post);

    //delete a user's post
    ResponseEntity deletePost(Long id, String stringToken);

    Iterable<Post> getUserPosts(String stringToken);
}
