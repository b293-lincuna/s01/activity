package com.zuitt.wdc044.controllers;

import com.zuitt.wdc044.models.Post;
import com.zuitt.wdc044.services.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

//https://origin.hibernate.org/orm/documentation/5.3/

@RestController
@CrossOrigin
public class PostController {

    @Autowired
    PostService postService;

    @RequestMapping(value="/posts", method = RequestMethod.POST)
    // ResponseEntity represents the whole HTTP response: status code, headers, and body.
    public ResponseEntity<Object> createPost(@RequestHeader(value = "Authorization") String stringToken, @RequestBody Post post){
        //We access the "postService" method and pass the following arguments:
        // stringToken of the current session which will be retrieved from the request headers.
        // a 'post' object will be instantiated upon receiving the request body and this will follow the properties set in the Post Model.
        postService.createPost(stringToken, post);

        return new ResponseEntity<>("Post created successfully", HttpStatus.CREATED);

    }

    @GetMapping("/posts")
    //@RequestMapping(value="/posts") //, method = RequestMethod.GET
    // ResponseEntity represents the whole HTTP response: status code, headers, and body.
    public ResponseEntity<Object> showPosts(){
        return new ResponseEntity<>(postService.getPosts(), HttpStatus.OK);
    }

    @RequestMapping(value="/posts/{postId}", method=RequestMethod.PUT)
    //@PathVariable as used for the data passed in the URL/endpoint.
    //@RequestParam VS @PathVariable
        // @RequestParam is used to extract data found in the query parameters (commonly used for filtering the results based on the condition.
        // @PathVariable is used to retrieve exact record
    public ResponseEntity<Object> updatePost(@PathVariable Long postId, @RequestHeader(value="Authorization") String stringToken, @RequestBody Post post){
       return postService.updatePost(postId, stringToken, post);
    }


    @RequestMapping(value="/posts/{postId}", method=RequestMethod.DELETE)
    public ResponseEntity<Object> deletePost(@PathVariable Long postId, @RequestHeader(value="Authorization") String stringToken){
        return postService.deletePost(postId, stringToken);
    }


    @GetMapping("/myPosts")
    public ResponseEntity<Object> myPosts(@RequestHeader(value="Authorization") String stringToken){
        return new ResponseEntity<>(postService.getUserPosts(stringToken),HttpStatus.OK);
    }
}
